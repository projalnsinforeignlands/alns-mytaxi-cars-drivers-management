package com.mytaxi.mytaxiserver.models;

import com.mytaxi.mytaxiserver.web.dtos.CarDTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;

@Entity
public class Car extends BaseEntity {

    @Column(name = "license_plate")
    private String licencePlate;

    @Column(name = "seat_count")
    private String seatCount;

    @Column
    private String convertible;

    @Column
    private String rating;

    @Enumerated
    @Column(name = "engine_type")
    private EngineTypeEnum engineType;

    public Car() {
    }

    public Car(CarDTO carDTO) {
        setAttributes(carDTO);
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public String getSeatCount() {
        return seatCount;
    }

    public void setSeatCount(String seatCount) {
        this.seatCount = seatCount;
    }

    public String getConvertible() {
        return convertible;
    }

    public void setConvertible(String convertible) {
        this.convertible = convertible;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public EngineTypeEnum getEngineType() {
        return engineType;
    }

    public void setEngineType(EngineTypeEnum engineType) {
        this.engineType = engineType;
    }

    public void setAttributes(CarDTO carDTO) {

        this.licencePlate = carDTO.getLicencePlate();

        this.seatCount = carDTO.getSeatCount();

        this.convertible = carDTO.getConvertible();

        this.rating = carDTO.getRating();

    }

}
