package com.mytaxi.mytaxiserver.repositories;

import com.mytaxi.mytaxiserver.models.Car;
import org.springframework.data.repository.CrudRepository;

public interface CarRepository extends CrudRepository<Car, Long> {

}
