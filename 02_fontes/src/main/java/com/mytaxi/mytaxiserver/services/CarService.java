package com.mytaxi.mytaxiserver.services;

import com.mytaxi.mytaxiserver.models.Car;
import com.mytaxi.mytaxiserver.repositories.CarRepository;
import com.mytaxi.mytaxiserver.web.dtos.CarDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;

@Service
public class CarService {

    @Autowired
    private CarRepository carRepository;

    public void create(Car car) {

        if (car == null) {
            // TODO: 2019-03-19 erro
        }

        car.setDateCreated(LocalDateTime.now().atZone(ZoneId.systemDefault()));

        carRepository.save(car);

    }

    public void update(Car car, CarDTO carDTO) {

        if (car == null) {
            // TODO: 2019-03-19 erro
        }

        car.setAttributes(carDTO);

        carRepository.save(car);

    }

    public void delete(Car car) {
        carRepository.delete(car);
    }

    public Optional<Car> readById(Long id) {

        Optional<Car> optionalCar = carRepository.findById(id);

        return optionalCar;

    }

    public Iterable<Car> readAll() {

        Iterable<Car> cars = carRepository.findAll();

        return cars;

    }

}
