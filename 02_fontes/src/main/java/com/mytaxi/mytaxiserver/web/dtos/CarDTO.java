package com.mytaxi.mytaxiserver.web.dtos;

import com.mytaxi.mytaxiserver.models.Car;

public class CarDTO extends BaseDTO {

    private String licencePlate;

    private String seatCount;

    private String convertible;

    private String rating;

    public CarDTO() {

    }

    public CarDTO(Car car) {

        this.setId(car.getId());

        this.setDateCreated(car.getDateCreated());

        this.licencePlate = car.getLicencePlate();

        this.seatCount = car.getSeatCount();

        this.convertible = car.getConvertible();

        this.rating = car.getRating();

    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public String getSeatCount() {
        return seatCount;
    }

    public void setSeatCount(String seatCount) {
        this.seatCount = seatCount;
    }

    public String getConvertible() {
        return convertible;
    }

    public void setConvertible(String convertible) {
        this.convertible = convertible;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

}
