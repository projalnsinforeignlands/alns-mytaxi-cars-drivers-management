package com.mytaxi.mytaxiserver.web.controllers;

import com.mytaxi.mytaxiserver.models.Car;
import com.mytaxi.mytaxiserver.services.CarService;
import com.mytaxi.mytaxiserver.web.dtos.CarDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;

@RestController
@RequestMapping(path = "/cars")
public class CarController {

    @Autowired
    private CarService carService;

    @RequestMapping(method = POST)
    public HttpEntity<?> create(@RequestBody CarDTO carDTO) {

        Car car = new Car(carDTO);

        carService.create(car);

        return ResponseEntity.ok().build();

    }

    @RequestMapping(method = PUT)
    public HttpEntity<?> update(@RequestBody CarDTO carDTO) {

        Optional<Car> optionalCar = carService.readById(carDTO.getId());

        if (!optionalCar.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Car car = optionalCar.get();

        carService.update(car, carDTO);

        return ResponseEntity.ok().build();

    }

    @RequestMapping(path = "/{id}", method = GET)
    public HttpEntity<?> readById(@PathVariable("id") Long id) {

        if (id == null) {
            // TODO: 2019-03-19 erro
        }

        return carService.readById(id)
                .map(payment -> ResponseEntity.ok(new CarDTO(payment)))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));

    }

    @RequestMapping(method = GET)
    public HttpEntity<?> readAll() {

        List<CarDTO> cars = StreamSupport.stream(carService.readAll().spliterator(), true)
                .map(car -> new CarDTO(car))
                .collect(Collectors.toList());

        return ResponseEntity.ok(cars);

    }

    @RequestMapping(path = "/{id}", method = DELETE)
    public HttpEntity<?> delete(@PathVariable("id") Long id) {

        if (id == null) {
            // TODO: 2019-03-19 erro
        }


        Optional<Car> optionalCar = carService.readById(id);

        if (!optionalCar.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Car car = optionalCar.get();

        carService.delete(car);

        return ResponseEntity.ok().build();

    }

}
