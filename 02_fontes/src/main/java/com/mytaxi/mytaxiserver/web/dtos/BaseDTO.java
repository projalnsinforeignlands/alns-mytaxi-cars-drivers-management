package com.mytaxi.mytaxiserver.web.dtos;

import java.time.ZonedDateTime;

public class BaseDTO {

    private Long id;

    private ZonedDateTime dateCreated;

    public BaseDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

}
